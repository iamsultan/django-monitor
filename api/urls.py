from django.conf.urls import patterns, include, url
from piston.resource import Resource
from api.handlers import ResultHandler, ResultHandlerByHash, NMapResultHandler

result_resource = Resource(ResultHandler)
result_byhash_resource = Resource(ResultHandlerByHash)
nmap_result_byhash_resource = Resource(NMapResultHandler)


urlpatterns = patterns('',
    url(r'^results/site/url/(?P<result_uri>[^/]+)/', result_resource),
    url(r'^results$', result_resource),
    url(r'^results/site/hash/(?P<result_hash>[^/]+)/',result_byhash_resource),
    url(r'^results/nmap/hash/(?P<result_hash>[^/]+)/',nmap_result_byhash_resource),
)

