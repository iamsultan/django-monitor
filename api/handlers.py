from piston.handler import BaseHandler
from InetRequest import Result
from NMapScan.models import NMapResult
from django.http import HttpResponse
import base64
import re

class NMapResultHandler(BaseHandler):
    allowed_methods = ('GET',)
    fields = ('hash','site','result')

    def read(self,request,result_hash=None):
        res = NMapResult.objects
        if result_hash:
            result = res.filter(hash=result_hash)
        if result:
            return result
        else:
            return HttpResponse('NMap Result not found with hash %s' % result_hash)

class ResultHandler(BaseHandler):
    allowed_methods = ('GET',)
    fields = ('errors', 'hash', 'iframe_tags', 'encoding', 'jscript_lst', 'timestamp', 'link_uri', 'object_tags',
              'header','html','ipaddrlist','img_tags','dump')


    def read(self,request,result_uri=None, result_hash=None):
        res = Result.objects

        if result_hash:
            result = res.filter(hash=result_hash)
            return result
        #result_uri = 'http://' + result_uri + '/'
        result_uri = result_uri.decode('utf8')
        print 'attempting query with url %s' % result_uri
        results = res.filter(title=result_uri)
        if results:
            return results
        else:
            return HttpResponse('Result not found with URL %s' % result_uri)

#    def create(self):
#        pass

#    def update(self):
#        pass

class ResultHandlerByHash(BaseHandler):
    allowed_methods = ('GET',)
    fields = ('errors', 'hash', 'iframe_tags', 'encoding', 'jscript_lst', 'timestamp', 'link_uri', 'object_tags',
              'header','html','ipaddrlist','img_tags','dump')

    def read(self,request,result_hash=None):
        res = Result.objects
        if result_hash:
            result = res.filter(hash=result_hash)
        if result:
            return result
        else:
            return HttpResponse('Result not found with hash %s' % result_hash)

            #    def create(self):
            #        pass

            #    def update(self):
            #        pass