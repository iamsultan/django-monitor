from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

import htmlgrab

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'htmlgrab.views.home', name='home'),
    # url(r'^htmlgrab/', include('htmlgrab.foo.urls')),

    url(r'^htmlgrab/$', 'htmlgrab.views.index'),
    url(r'^htmlgrab/link$', 'htmlgrab.views.link'),
    url(r'^htmlgrab/scan$', 'htmlgrab.views.scan'),
    url(r'^htmlgrab/scan/(?P<scan_id>\d+)/$', 'htmlgrab.views.scan_detail'),
    url(r'^htmlgrab/schedule$', 'htmlgrab.views.schedule'),
    url(r'^htmlgrab/variable$', 'htmlgrab.views.variable'),
    url(r'^htmlgrab/proxy$', 'htmlgrab.views.proxy'),
    url(r'^htmlgrab/provider$', 'htmlgrab.views.provider'),
    url(r'^api/',include('djm.api.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
     url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:

     url(r'^admin/', include(admin.site.urls)),

    #TO-DO: Edit this for later. We don't want to be serving up thing in
    #MEDIA_ROOT for just any user in a production environment
    (r'^', include('htmlgrab.urls')),


) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
