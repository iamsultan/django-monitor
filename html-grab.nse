description = [[Calls django-monitor's htmlgrab app and attempts to get the HTML with specified options.]]

categories = {"safe"}

require "stdnse"

function portrule(host,port)
	return port.number == 80 or port.number == 443
end

function action(host,port)
	
	local url = stdnse.get_script_args("url")
	local proxy = stdnse.get_script_args("proxy")

	print 'In function action'

	if url ~= nil then 
		if proxy ~= nil then
			print 'Proxy argument detected'
			os.execute("python htmlgrab_cli.py "..url.." ".."-p "..proxy)
		else
			os.execute("python htmlgrab_cli.py "..url)
		end
	else
		print("Error with html-grab.  URL not specified.")
	end
	
end
