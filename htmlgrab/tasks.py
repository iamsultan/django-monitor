from celery.task import task
from datetime import datetime, date, timedelta
from htmlgrab.models import HTMLRequest
from InetRequest import Link, ProxyServer
#TO-DO: Change this when I move the project off my laptop

class QException(BaseException):
    pass

@task(name='update-ips')
def update_ips():
    links = Link.objects.all()
    for l in links:
        print 'Updating site %s' % l.__str__()
        l.get_ip_list()


@task(name='html-grab-job')
def html_grab_job(num_links, *args):
    some_links = []
    if num_links == "all":
        some_links = Link.objects.all()
    elif num_links == "these":
        for arg in args:
            sl = Link.objects.filter(title=arg)
            if sl:
                some_links.append(sl[0])
    else:
        some_links = Link.objects.all()[:num_links]

    for l in some_links:
        htmr = HTMLRequest(proxy_info=None)
        htmr.set_uri(link=l)
        htmr.perform()

@task(name='html-grab-job-proxy')
def html_grab_job_proxy(num_links, proxy, *args):
    some_links=[]
    print "in html_grab_proxy"
    if num_links == "all":
        some_links = Link.objects.all()[:num_links]
    elif num_links == "these":
        for arg in args:
            sl = Link.objects.filter(title=arg)
            if sl:
                some_links.append(sl[0])
    else:
        some_links = Link.objects.all()[:num_links]
    pserver = ProxyServer.objects.filter(proxy_name=proxy)
    if len(pserver) > 0:
        ps = pserver[0]
    else:
        raise QException("Error: Proxy with specified name not found.")

    print "collected links and proxy server"
    for l in some_links:
        htmr = HTMLRequest(proxy_info=ps)
        htmr.set_uri(link=l)
        htmr.perform()

@task(name='sum')
def add(x, y):
    return x + y

