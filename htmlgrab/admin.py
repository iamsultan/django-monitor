from django.contrib import admin
#from htmlgrab.models import *
from InetRequest import *
import base64

admin.site.register(Link)

#class ResultAdmin(admin.ModelAdmin):
#    def _decodedHTML(self,obj):
#        str =  base64.decodestring(obj.html)
#        if obj.encoding == 'ISO-8859-1':
#            str = str.decode('cp1251').encode('utf-8')
#        return str
#
#    def _decodedJscript(self,obj):
#        str = base64.decodestring(obj.jscript_list)
#        if obj.encoding == 'ISO-8859-1':
#            str = str.decode('cp1251').encode('utf-8')
#        return str
#
#    list_display = ['link', 'link_uri', 'aliaslist', 'ipaddrlist', '_decodedHTML', '_decodedJscript', 'hash', 'header', 'screenshot', 'dump']

class UserAgentAdmin(admin.ModelAdmin):
    list_display = ['short_desc']

admin.site.register(Result)
#admin.site.register(IRequest)
admin.site.register(Provider)
admin.site.register(ProxyServer)
admin.site.register(UserAgent,UserAgentAdmin)
