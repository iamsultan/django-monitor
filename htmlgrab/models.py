from django.db import models
from urlparse import urljoin, urlparse
from Exceptions import ProxyUsageImpossible

from InetRequest import IRequest

import os
import time
import socket
import base64
import requests
import cStringIO
import re, HTMLParser
import urllib2
import datetime

from django.conf import settings

#import InetRequest as INETR
from bs4 import BeautifulSoup
from collections import defaultdict
from requests.exceptions import SSLError
from helpers import VerifiedHTTPSHandler
from urlparse import urlparse
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver import FirefoxProfile



# Create your models here.

socket.setdefaulttimeout(120)

UNABLE_CONNECT_TO_HOST = -1
NO_CONTENT = '[NO_CONTENT]'
HTMLParser.attrfind = re.compile(
    r'\s*([a-zA-Z_][-.:a-zA-Z_0-9]*)(\s*=\s*'
    r'(\'[^\']*\'|"[^"]*"|[^\s>^\[\]{}\|\'\"]*))?')

#converts the list tuples returned by urllib2.urlopen(...).items() to the standard dictionary format that requests uses
def tuple_to_dict(lst):
    d = defaultdict(list)
    for k, v in lst:
        d[k].append(v)
    x = {k:d[k][0] for k in d}
    return x


class HTMLRequest(IRequest):
    def __init__(self, proxy_info):
        IRequest.__init__(self, proxy_info=proxy_info)

    # The actual request maker
    #TO-DO: Add https?
    def request(self, head=False):
        headers = {}
        content = cStringIO.StringIO()
        jscript_lst = None
        img_tags = ''
        object_tags = ''
        iframe_tags = ''
        encoding = ''
        errors = ''
        graburl = urlparse(self.link.link.__str__())
        try:
            #If we're making the request with a proxy, make it with the proxy
            #settings


            if self.proxy_object:
                print "making proxy request with proxy object %s" % self.proxy_object
                if graburl.scheme == 'https':
#                    link_url = self.link.link.__str__()
#                    https_url = urlparse(link_url)
#                    https_url = 'https://' + https_url.netloc + https_url.path + https_url.params + https_url.query + https_url.fragment
                    print "for site %s" % self.link.link.__str__()
                    print "In HTTPS-over-proxy condition"
                    #response = requests.get(url=self.link.link.__str__(), verify=True, proxies={"https": self.proxy_object})
                    if self.verifycert == True:
                        opener = urllib2.install_opener(urllib2.build_opener(VerifiedHTTPSHandler(),urllib2.ProxyHandler({'https': self.proxy_object})))
                    else:
                        opener = urllib2.install_opener(urllib2.build_opener(urllib2.HTTPSHandler(),urllib2.ProxyHandler({'https': self.proxy_object})))

                    response = urllib2.Request(url=self.link.link.__str__())
                    response = urllib2.urlopen(response)
                    html = response.read()
                    #print 'HTML retrieval successful'
                    headers = tuple_to_dict(response.info().items())
                    html_soup = BeautifulSoup(html)
                    encoding = html_soup.original_encoding
                    jscript_lst, errors = self.grep_for_scripts_and_eval(response.url,html, self.proxy_object, self.verifycert)
                    img_tags = self.grep_for_tag('img',html)
                    iframe_tags = self.grep_for_tag('iframe',html)
                    object_tags = self.grep_for_tag('object',html)

                    extras = {
                        'jscripts': jscript_lst,
                        'imgs': img_tags,
                        'iframes': iframe_tags,
                        'objects': object_tags
                    }

                    return headers, html, extras, encoding, errors

                else:
                    print "for site %s" % self.link.req_format()
                    #print 'in https-no-verification condition'
                    response = requests.get(url=self.link.req_format(), proxies={"http": self.proxy_object})
                    encoding = response.encoding
                    headers = response.headers
            else:
                if graburl.scheme == 'https':
                    print "making direct HTTPS request for %s" % self.link.link.__str__()
#                    link_url = self.link.link.__str__()
#                    https_url = urlparse(link_url)
#                    https_url = 'https://' + https_url.netloc + https_url.path + https_url.params + https_url.query + https_url.fragment
                    print "for site %s" % self.link.link.__str__()
                    response = requests.get(url=self.link.link.__str__(), verify=True)
                    encoding = response.encoding
                    headers = response.headers
                else:
                    print "making direct request for %s" % self.link.req_format()
                    response = requests.get(url=self.link.req_format())
                    encoding = response.encoding
                    headers = response.headers

            #grab the headers from the request


            html = None
            #If it returned a 200
            if response.ok:
                headers.setdefault('status', response.status_code)
                html = response.content
                #if response.encoding == 'ISO-8859-1':
                #    html = handle_utf8_encoding(html)
                    #Add grep of html content for embeddded javascript:
                    #There's two types we need here:
                    #1. Scripts embedded on an external site
                    #2. Scripts embedded in a local directory
                jscript_lst, errors = self.grep_for_scripts_and_eval(response.url,html, self.proxy_object, self.verifycert)
                img_tags = self.grep_for_tag('img',html)
                iframe_tags = self.grep_for_tag('iframe',html)
                object_tags = self.grep_for_tag('object',html)
                if self.link.dump and head is False:
                    content.write(response.content)
                else:
                    content.write(NO_CONTENT)
            else:
                html = response.content
                headers.setdefault('status', response.status_code)
                content.write(NO_CONTENT)
        except requests.ConnectionError as ex:
            errors =  "could not connect to %s" % self.link.link.__str__()
            return (
                '',

                ex.__unicode__(),None,'',errors
            )
        except requests.HTTPError as ex:
            errors = "could not connect to %s" % self.link.link.__str__()
            return (
                '',
                ex.__unicode__(),None,'',errors
                )
        except HTMLParser.HTMLParseError as htmlperr:
            errors = 'In Javascript retrieval: ' + htmlperr.msg + ' ' + str(htmlperr.lineno) + ' ' + str(htmlperr.offset)
            return ('',htmlperr.__unicode__(),None,'',errors)
        except SSLError as ex:
            errors = "Invalid SSL Certificate: %s" % ex.__str__()

        extras = {
            'jscripts': jscript_lst,
            'imgs': img_tags,
            'iframes': iframe_tags,
            'objects': object_tags
        }

        return headers, html, extras, encoding, errors

    def grep_for_scripts_and_eval(self, siteurl ,html_content, proxy, verifycert):
        errors=''
        reqc = ''
        try:
            html_content_soup = BeautifulSoup(html_content)
            htmlcs_srcs = [tag.attrs['src'] for tag in html_content_soup.findAll('script', src=True)]
            jscripts = cStringIO.StringIO()
            jscripts.write('*****')
            for src in htmlcs_srcs:
                srcurl = urljoin(siteurl,src)
                srcurl = urlparse(srcurl)
                if proxy:
                    try:
                        print 'In grep_for_scripts_for_eval: grabbing javascript at %s' % srcurl.geturl()
                        if srcurl.scheme == 'https':
                            if verifycert == True:
                                opener = urllib2.install_opener(urllib2.build_opener(VerifiedHTTPSHandler(),urllib2.ProxyHandler({'https': self.proxy_object})))
                            else:
                                opener = urllib2.install_opener(urllib2.build_opener(urllib2.HTTPSHandler(),urllib2.ProxyHandler({'https': self.proxy_object})))

                            response = urllib2.Request(url=srcurl.geturl())
                            response = urllib2.urlopen(response)
                            reqc = response.read()

#                    req = requests.get(url=srcurl, verify=True, proxies={"https": proxy})
                        else:
                            req = requests.get(url=srcurl.geturl(), proxies={"http": proxy})
                            reqc = req.content
                    except SSLError as ssler:
                        jscripts.write('(Scripts retrieved via HTTP due to SSLError)')
                        req = requests.get(url=srcurl.geturl(), proxies={"http": proxy})
                        reqc = req.content
                else:
                    try:
                        print 'In grep_for_scripts_for_eval: grabbing javascript at %s' % srcurl.geturl()
                        if srcurl.scheme == 'https':
#                            link_url = self.link.link.__str__()
#                            https_url = urlparse(link_url)
#                            https_url = 'https://' + https_url.netloc + https_url.path + https_url.params + https_url.query + https_url.fragment
                            req = requests.get(url=srcurl.geturl(), verify=True)
                        else:
                            req = requests.get(url=srcurl.geturl())
                        reqc = req.content
                    except SSLError as ssler:
                        jscripts.write('(Scripts retrieved via HTTP due to SSLError)')
                        req = requests.get(url=srcurl.geturl(), proxies={"http": proxy})
                        reqc = req.content
                jscripts.write(reqc)
                jscripts.write('*****')
                #print reqc
                #if req.encoding == 'ISO-8859-1':
                #    jscripts.write() = reqc.decode('cp1251').encode('UTF-8', errors='ignore')
                #else:
                #    jscript = reqc
                #jscript_lst.append(jscript)
            return jscripts, errors
        except HTMLParser.HTMLParseError as htmlperr:
            return None, 'In Javascript retrieval: ' + htmlperr.msg + ' ' + str(htmlperr.lineno) + ' ' + str(htmlperr.offset)

    def grep_for_tag(self,tag_name, html):
        html_content_soup = BeautifulSoup(html)
        tag_str = cStringIO.StringIO()
        html_tags = [inst for inst in html_content_soup.findAll(tag_name)]
        tag_str.write(html_tags.__str__())
        return tag_str


    # Builds opener
    #TO-DO: Add HTTPS functionality?
    def set_opener(self, urlscheme, proxy):
        if self.use_proxy and proxy:
            #If we're not using a proxy, we just want to build a request to
            #the site and specify the port.
            if not proxy.user or not proxy.password:
                if urlscheme == True:
                    scheme = '%s://%s:%s' % (
                        'https',
                        proxy.host.decode('ascii'),
                        proxy.port
                        )
                    print "Created HTTPS proxy object %s" % scheme
                else:
                    scheme = '%s://%s:%s' % (
                        'http',
                        proxy.host.decode('ascii'),
                        proxy.port
                        )
                    print "Created HTTP proxy object %s" % scheme
            else:
                #Otherwise we need to add data like username and password
                #Do we really want to disclose the password over http, which
                #is plaintext?
                scheme = '%s://%s:%s@%s:%s' % (
                    'http',
                    proxy.user,
                    proxy.password,
                    proxy.host,
                    proxy.port
                    )

            #print 'Setting proxy: ', scheme

            self.proxy_object = scheme

        elif not self.use_proxy and proxy:
            # if don't use proxy but proxy object was passed
            raise ProxyUsageImpossible('Unable to use proxy.')
        elif not proxy:
            #print 'Setting default opener'
            # if proxy is None the set default opener
            self.proxy_object = None
            print "in set_opener, proxy wasn't set"


    def take_screenshot(self, site, url, rdate, user_agent,proxy=None):
        print 'Grabbing screenshot'

        #Set up the virtual display and the final file name
        fname = getattr(settings,'MEDIA_ROOT') + url + '-' + str(datetime.datetime.now()) + '.png'
        display = Display(visible=0, size=(800, 600))
        display.start()

        profile = FirefoxProfile()
        profile.set_preference('general.useragent.override',user_agent.user_agent_string)
        if proxy:
            profile.set_preference("network.proxy.type", 1)
            profile.set_preference("network.proxy.http", proxy.host)
            profile.set_preference("network.proxy.http_port", proxy.port)
            profile.set_preference("network.proxy.ssl", proxy.host)
            profile.set_preference("network.proxy.ssl_port", proxy.port)
            profile.update_preferences()

        browser = webdriver.Firefox(profile)
        browser.get(site)
        browser.save_screenshot(fname)
        browser.quit()
        display.stop()
        print 'Screenshot completed'

        return fname
