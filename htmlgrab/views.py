# Create your views here.
from django.template import RequestContext
from django.shortcuts import render_to_response
from InetRequest import Result
from django.http import HttpResponse

# Create your views here.

def index(request):
    return render_to_response('djmonitor/index.html', {}, context_instance=RequestContext(request))

def link(request):
    return render_to_response('djmonitor/link.html', {}, context_instance=RequestContext(request))

def scan(request):
    return render_to_response('djmonitor/scan.html', {}, context_instance=RequestContext(request))

def scan_detail(request, scan_id):
    #scan detailed data retrieval here, adding a stub for now
    scan_data = 1
    return render_to_response('djmonitor/scan_detail.html', {'scan_data' : scan_data}, context_instance=RequestContext(request))

def proxy(request):
    return render_to_response('djmonitor/proxy.html', {}, context_instance=RequestContext(request))

def provider(request):
    return render_to_response('djmonitor/provider.html', {}, context_instance=RequestContext(request))

def schedule(request):
    return render_to_response('djmonitor/schedule.html', {}, context_instance=RequestContext(request))

def variable(request):
    return render_to_response('djmonitor/variable.html', {}, context_instance=RequestContext(request))

def listview(request):
    r = Result.objects.filter(pk=pk)
    img = r[0].screenshot.open(mode='rb')
    return HttpResponse(img, mimetype='image/png')
