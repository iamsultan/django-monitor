__author__ = 'jdoe'

class NoLinks(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class ConfigNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class ProxyNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class ProxyUsageImpossible(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)
