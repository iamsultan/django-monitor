from celery.task import task
from InetRequest import Link
from models import NMapScan, NMapResult
from datetime import datetime

import nmap
import cStringIO
import hashlib
import os, sys

@task(name='nmap-scan')
def nmap_scan_job(nm_scan, *args):
    #Get the NMapScan object named in nm_scan and create a new NMapResult
    #object to store the results.
    scanners = NMapScan.objects.filter(name=nm_scan)
    scan_result = NMapResult()
    #results = []
    if len(scanners) > 0:

        #Get the scanner object we want, set up the port scanner, and
        #get the sites we want to scan
        scanner = scanners[0]
        portscanner = nmap.PortScanner()
        links_to_scan = scanner.links.all()

        for link in links_to_scan:
            #Since we might be dealing with a site that has more than one
            #IP address (and whatever server is at each IP address might have
            #a different configuration), we scan each IP address associated
            #with the site.
            print 'scanning site %s\n' % link.title
            for ip in eval(link.ipaddrlist):
                #perform the scan, but first we want to check if we need root
                #to do it properly.  This might not solve all cases, but we can
                #revise it in later versions.
                if scanner.args.find('-O') >= 0:
                        print 'OS fingerprinting requires root.  Prompting for password'
                        args = ['sudo', sys.executable] + sys.argv + [os.environ]
                        os.execlpe('sudo', *args)
                result_dict = portscanner.scan(hosts=ip, ports=scanner.ports, arguments=scanner.args)
                print 'scanning IP %s' % ip
                #Prepare a summary string so the user doesn't have to wade
                #through the giant dictionary returned by python-nmap
                result = cStringIO.StringIO()
                result.write('***' + link.title + ' at IP: ' + ip + '***' + '\n')
                result.write('State: ' + portscanner[ip].state() + '\n')
                result.write('Ports' +'\n')
                result.write('-----------' +'\n')

                #for tcp/idp scans
                for proto in portscanner[ip].all_protocols():
                    result.write('Protocol: ' + proto)
                    #Get the list of open/filtered ports and sort them
                    lport = portscanner[ip][proto].keys()
                    lport.sort()

                    # This will give us a string like so
                    #port: 22 state: open name: ssh
                    for port in lport:
                        result.write ('port : ' + str(port) + '   state : ' + portscanner[ip][proto][port]['state'] + '    name: ' + portscanner[ip][proto][port]['name'] +  '\n')
                result.write('-----------' + '\n')

            #Log the necessary results and data
            #Create a unique hash from the time
            scan_result.result = result_dict
            scan_result.summary = result.getvalue()
            scan_result.site = link.title.__str__()

            #Create a unique hash from the time and save in the database
            m = hashlib.md5()
            m.update(datetime.now().__str__())
            scan_result.hash =  m.hexdigest()
            scan_result.save()

            #Create a new NMapResult object for the next link
            scan_result = NMapResult()

    else:
        print 'Error: NMapScan not found'
#     some_links = []
#     if num_links == "all":
#        some_links = Link.objects.all()
#     elif num_links == "these":
#         for arg in args:
#             sl = Link.objects.filter(title=arg)
#             if sl:
#                some_links.append(sl[0])
#     else:
#         some_links = Link.objects.all()[:num_links]
#
#     for l in some_links:
