__author__ = 'jdoe'

#Note 3-9-12: Editing out scan object to integrate with celery scheduling
#jobs.

from django.db import models
from annoying.fields import JSONField
#from datetime import time
from urlparse import urlparse
from datetime import datetime
from django.db import models
from django.core.files.storage import FileSystemStorage
from django.db.models import ImageField
from django.core.files import File

import socket
import base64
import simplejson
import hashlib
import cStringIO
from settings import *

screenshot_path = ''

def strip_newlines(strng):
    split_str = strng.split('\n')
    final_str = ''
    for enc_str in split_str:
        final_str = final_str + enc_str
    return final_str

class IRequest(models.Model):
    def __init__(self, proxy_info):
        self.proxy = proxy_info
        self.verifycert = False
        if proxy_info:
            self.use_proxy = proxy_info.use
            if proxy_info.https == True:
                if proxy_info.verifycert == True:
                    self.verifycert = True
                    print "HTTPS enabled and certificate verification enabled"
                else:
                    print "HTTPS enabled, certificate verification disabled"
            else:
                self.https = False
                print "HTTPS disabled"
        else:
            self.use_proxy=None

    #schedule parameter removed since we're not dealing with schedules yet
    #Scan parameter removed, see note at top of file
    def perform(self):
        #self.set_opener(self.proxy)
        direct_html = ''
        proxy_html = ''
        direct_headers = ''
        urlscheme = urlparse(self.link.link.__str__()).scheme
        screenshot_path = ''
        proxy_headers = ''
        #The type of scan we want to perform
 #       self.scan = scan

        #If we're not doing this with a proxy,
        #perform the DNS resolution and make the request
        proxy_headers, proxy_content = ('', '')
        ip_struct = self.get_ip(self.link.link.__str__())

        #handles the above logic if we're using a proxy
        if self.use_proxy:
            print "Proxy argument detected.  Setting opener"
            self.set_opener(urlscheme,self.proxy)
            proxy_headers, proxy_html, proxy_extras, proxy_encoding, proxy_errors = self.request()
        else:
            proxy=None
            self.set_opener(urlscheme,self.proxy)
            direct_headers, direct_html, direct_extras, direct_encoding, direct_errors = self.request()
        #if the DNS resolution failed, set it to null data
        if len(ip_struct) < 1:
            ip_struct = [None, None, None]

        #create a new Result from the scan and set up its data
        #schedule set to None because we're not dealing with schedules yet
        result = Result()
  #      result.scan = scan
        result.schedule = None
        result.aliaslist = str(ip_struct[1])
        result.ipaddrlist = str(ip_struct[2])
        result.link = self.link
        result.link_uri = self.link.req_format()
        result.title = self.link.title
        rdate = ''
        if direct_html or proxy_html:
            if direct_html:
                if direct_headers <> '':
                    result.html = simplejson.dumps(strip_newlines(base64.encodestring(direct_html)))
                    print "Saving HTML and Javascript for %s from direct request" % self.link.req_format()
                    if direct_extras['jscripts']:
                        result.jscript_lst = simplejson.dumps(strip_newlines(base64.encodestring(direct_extras['jscripts'].getvalue())))
                    else:
                        result.jscript_lst = ''

                    if direct_extras['imgs']:
                        result.img_tags = simplejson.dumps(strip_newlines(base64.encodestring(direct_extras['imgs'].getvalue())))
                    else:
                        result.img_tags = ''

                    if direct_extras['iframes']:
                        result.iframe_tags = simplejson.dumps(strip_newlines(base64.encodestring(direct_extras['iframes'].getvalue())))
                    else:
                        result.img_tags = ''

                    if direct_extras['objects']:
                        result.object_tags = simplejson.dumps(strip_newlines(base64.encodestring(direct_extras['objects'].getvalue())))
                    else:
                        result.object_tags = ''

                    timestamp = datetime.now()

                    if self.link.screenshot == True:
                        screenshot_path = self.take_screenshot(result.link_uri,self.link.title,timestamp,UserAgent.objects.filter(pk=self.link.user_agent_id)[0])


                    result.encoding = direct_encoding
                    result.errors = direct_errors
                else:
                    result.html = simplejson.dumps(strip_newlines(base64.encodestring(direct_html)))
                    result.jscript_lst = ''
                    result.img_tags = ''
                    result.img_tags = ''
                    result.object_tags = ''
                    rdate = datetime.now().__str__()
                    result.encoding = ''
                    result.errors = direct_errors
            elif proxy_html:
                if proxy_headers <> '':
                    result.html = simplejson.dumps(strip_newlines(base64.encodestring(proxy_html)))
                    print "Saving Javascript and HTML for %s from proxy" % result.link_uri
                    if proxy_extras['jscripts']:
                        result.jscript_lst = simplejson.dumps(strip_newlines(base64.encodestring(proxy_extras['jscripts'].getvalue())))
                    else:
                        result.jscript_lst = ''

                    if proxy_extras['imgs']:
                        result.img_tags = simplejson.dumps(strip_newlines(base64.encodestring(proxy_extras['imgs'].getvalue())))
                    else:
                        result.img_tags = ''

                    if proxy_extras['iframes']:
                        result.iframe_tags = simplejson.dumps(strip_newlines(base64.encodestring(proxy_extras['iframes'].getvalue())))
                    else:
                        result.img_tags = ''

                    if proxy_extras['objects']:
                        result.object_tags = simplejson.dumps(strip_newlines(base64.encodestring(proxy_extras['objects'].getvalue())))
                    else:
                        result.object_tags = ''

                    #screenshot here

                    if self.link.screenshot == True:
                        timestamp = datetime.now()
                        screenshot_path = self.take_screenshot(result.link_uri,self.link.title,timestamp,UserAgent.objects.filter(pk=self.link.user_agent_id)[0],self.proxy)
                        #result.screenshot.save(screenshot_path,File(open(screenshot_path)))


                    rdate = datetime.now()
                    result.timestamp = rdate
                    rdate = rdate.__str__()
                    result.encoding = proxy_encoding
                    result.errors = proxy_errors
                else:
                    print "Saving HTML for %s from proxy" % result.link_uri
                    result.html = simplejson.dumps(strip_newlines(base64.encodestring(proxy_html)))
                    result.jscript_lst = ''
                    result.img_tags = ''
                    result.img_tags = ''
                    result.object_tags = ''
                    rdate = datetime.now().__str__()
                    result.encoding = ''
                    result.errors = proxy_errors
        result.header = {
            'direct': direct_headers,
            'proxy': proxy_headers
        }



        #try:
        #    result.dump = {
        #        'direct': base64.encodestring(direct_content.getvalue()),
        #        'proxy': base64.encodestring(proxy_content.getvalue())
        #    }
        #except AttributeError:
        result.dump = {
           'direct': '',
           'proxy': ''
        }



        hstring = rdate + result.link_uri.encode('ascii')
        m = hashlib.md5()
        m.update(hstring)
        result.hash = m.hexdigest()
        result.save()
        if self.link.screenshot == True:
            result.screenshot.save(screenshot_path,File(open(screenshot_path)))


    def request(self, head=False):
        pass

    #Performs a DNS resolution to determine the IP(s) for
    #the site we're trying to request/scan
    def get_ip(self, link=None):
        if link:
            parsed_url = urlparse(url=link)

            # gethostbyname_ex(host) -> (name, aliaslist, addresslist)
            # Return the true host name, a list of aliases, and a list of IP addresses, for a host.
            # The host argument is a string giving a host name or IP number.
            try:
                if parsed_url.netloc:
                    to_get = parsed_url.netloc
                elif parsed_url.path:
                    to_get = parsed_url.path

                return socket.gethostbyname_ex(to_get)
            except socket.gaierror:
                return []

    class Meta:
        app_label = 'htmlgrab'

    def set_uri(self, link=object):
        if link:
            if 'http://' not in link.link.lower() and 'https://' not in link.link.lower():
                link.link = 'http://%s' % link.link
            self.link = link

    def set_opener(self, urlscheme, proxy):
        pass


class Link(models.Model):
    title = models.CharField(max_length=100)
    link = models.URLField(unique=True)
    ipaddrlist = models.TextField()
    port = models.IntegerField(max_length=4,default=80)

    # settings
    screenshot = models.BooleanField(default=False)
    dump = models.BooleanField(default=False)
    fullcheck = models.BooleanField(default=True)

    user_agent=models.ForeignKey('UserAgent')

    class Meta:
       app_label = 'htmlgrab'

    def req_format(self):
        parsed_uri = urlparse(self.link)
        site_port = self.port
        if site_port == None:
            if parsed_uri.scheme == 'http':
                site_port = 80
            else:
                site_port = 443

        return parsed_uri.scheme + '://' + parsed_uri.netloc + ':' + str(site_port) + parsed_uri.path + parsed_uri.params + parsed_uri.query + parsed_uri.fragment

    #Performs a DNS resolution to determine the IP(s) for
    #the site we're trying to request/scan
    def get_ip_list(self):
        parsed_url = urlparse(self.link.__str__())
        # gethostbyname_ex(host) -> (name, aliaslist, addresslist)
        # Return the true host nl.ame, a list of aliases, and a list of IP addresses, for a host.
        # The host argument is a string giving a host name or IP number.
        try:
            if parsed_url.netloc:
                to_get = parsed_url.netloc
            elif parsed_url.path:
                to_get = parsed_url.path

            self.ipaddrlist = socket.gethostbyname_ex(to_get)[2]
            self.save()
        except socket.gaierror:
            self.ipaddrlist = []
            return


    def __unicode__(self):
        return self.title + ': ' + self.link

class UserAgent(models.Model):
    short_desc = models.CharField(max_length=20)
    user_agent_string = models.CharField(max_length=512)

    class Meta:
        app_label= 'htmlgrab'

    def __unicode__(self):
        return self.short_desc + ' : ' + self.user_agent_string

class Provider(models.Model):
    name = models.CharField(max_length=20)
    ip_range = models.IPAddressField(max_length=120)

    class Meta:
       app_label = 'htmlgrab'

    def __unicode__(self):
        return 'Provider: %s; IP: %s' % (self.name, self.ip_range,)

class ProxyServer(models.Model):
    host = models.CharField(max_length=100)
    proxy_name = models.CharField(max_length=30)
    port = models.IntegerField(max_length=4)
    user = models.CharField(max_length=40, blank=True)
    password = models.CharField(max_length=40, blank=True)
    use = models.BooleanField()
    https = models.BooleanField(verbose_name='Use HTTPS?')
    verifycert = models.BooleanField(verbose_name='If HTTPS, Verify Certificate?', default=True)

    class Meta:
        app_label = 'htmlgrab'

    def __unicode__(self):
        return self.host

    def http(self):
        return 'http://' + self.host + ':' + self.port.__str__() + '/'

    def __as_json__(self):
        return {
            'id': self.pk,
            'host': self.host,
            'port': self.port,
            'user': self.user,
            'password': self.password,
            'use': self.use
        }

#class Scan(models.Model):
#    title = models.CharField(max_length=40)
#    provider = models.ForeignKey(to=Provider, null=True)
#    proxy = models.ForeignKey(to=ProxyServer, null=True)
#    start_date = models.DateTimeField(auto_now=True)
#    end_date = models.DateTimeField(null=True)
#    num_links = models.IntegerField(max_length=4)
#    #priority = models.SmallIntegerField(max_length=1, default=PRIO_NORMAL)
#
#    class Meta:
#        app_label = 'htmlgrab'
#
##    def save(self, **kwargs):
##        diff = (self.end_date - datetime.now())
##        diff = diff.total_seconds()
##        links = Link.objects.all()[:self.num_links]
##        #send_task('html_grab_job',[self.proxy.__as_json__(),self,links])
##        html_grab_job.apply_async(args=[self.proxy.__as_json__(),self,links], countdown=diff)
##        super(Scan, self).save(**kwargs)
#
#    def __unicode__(self):
#        return '#%d Scan at: %s' % (self.pk, self.start_date)


#class Schedule(models.Model):
#    title = models.CharField(max_length=40)
#    date_from = models.DateTimeField(null= True)
#    date_to = models.DateTimeField(null=True)
#    last_start = models.DateTimeField(null=True)
#    frequency = models.IntegerField(default=ONCE)
#    periods = models.IntegerField(default=EVERY_DAY)
#
#    class Meta:
#        app_label = 'htmlgrab'

#class ScheduleList(models.Model):
#    scan = models.ForeignKey(to=Scan, null=True)
#    schedule = models.ForeignKey(to=Schedule)
#    link = models.ForeignKey(to=Link)
#
#    class Meta:
#        app_label = 'htmlgrab'
#
#    def __unicode__(self):
#        return 'List id: %d' % (self.pk,)

#class ScheduleProxy(models.Model):
#    schedule = models.ForeignKey(to=Schedule)
#    proxy = models.ForeignKey(to=ProxyServer)
#
#    class Meta:
#        app_label = 'htmlgrab'

class Result(models.Model):
#    scan = models.ForeignKey(to=Scan)
#    schedule = models.ForeignKey(to=Schedule, null=True)
    link = models.ForeignKey(to=Link)
    link_uri = models.URLField(max_length=100)
    aliaslist = models.CharField(max_length=255)
    ipaddrlist = models.CharField(max_length=255)
    html = models.TextField()
    jscript_lst = models.TextField()
    hash = models.CharField(max_length=33)
    encoding = models.CharField(max_length=30)
    errors = models.TextField()
    iframe_tags = models.TextField()
    img_tags = models.TextField()
    object_tags = models.TextField()
    timestamp = models.DateTimeField()
    title = models.CharField(max_length=50)

    header = JSONField() # { 'direct': HEADERS, 'proxy': HEADERS }
    #screenshot = JSONField() # { 'direct': PATH, 'proxy': PATH }
    screenshot = ImageField(upload_to='screenshots/%Y/%m/%d')
    dump = JSONField() #  { 'direct': DUMP, 'proxy': DUMP }

    #TO-DO: Create a subclass called HTMLGrabResult that injerits from this with its
    #and move the export_to_xml method down there.

    #Exports the results of the scan to XML
    #Used
    def export_to_xml(timestamp):
        pass

    class Meta:
        app_label = 'htmlgrab'

    def __unicode__(self):
        return 'Result for %s' % self.link_uri

#class Variable(models.Model):
#    key = models.CharField(max_length=20, verbose_name='Variable')
#    value = models.CharField(max_length=1024)
#
#    class Meta:
#        app_label = 'htmlgrab'
#
#    def __unicode__(self):
#        return '%s: %s' % (self.key, self.value,)
